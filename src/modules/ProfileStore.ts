import store from '@/store';
import {IProfile} from '@/interfaces/IProfile';

export const ProfileStore = {
    namespaced: true,
    state     : {
        profile: {},
    },
    mutations : {
        inc(state: { count: number }) {
            // `state` указывает на локальное состояние модуля
            state.count++;
        },
    },
    actions   : {
        async loadProfile() {
            const [scope, data] = arguments;
            const query         = 'query{ \
                    getProfile(id: ' + data.id + '){\
                        id\
                        name\
                        clothes{\
                            name\
                        }\
                        hairstyle{\
                            name\
                        }\
                        affectations{\
                            name\
                        }\
                        ethnicOrigins{\
                            name\
                        }\
                        stats {\
                            int\
                            ref\
                            cool\
                            tech\
                            luck\
                            attr\
                            ma\
                            emp\
                            body\
                            run\
                            leap\
                            lift1\
                            lift2\
                        }\
                        motivationsLog{\
                            id\
                            name\
                            motivation{\
                                name\
                            }\
                        } \
                        fbLog{\
                            id\
                            name\
                            fb{\
                                name\
                            }\
                        }\
                        lifeEvents {\
                            id\
                            name\
                            bpBwLog{\
                                name\
                                bpBw{\
                                    name\
                                }\
                            }\
                            faeLog{\
                                name\
                                fae{\
                                    name\
                                }\
                            }\
                            romanticLifeLog{\
                                name\
                                romanticLife{\
                                    name\
                                }\
                            }\
                        }\
                        siblings{\
                            name\
                            age\
                            sex\
                            relationship\
                        }\
                    }\
                }';

            const resp = await store.dispatch('getEntity', query);
            scope.state.profile = resp.data.data.getProfile;
        },

        postProfile(): Promise<{}> {
            const name  = arguments[1].name;
            const query = 'mutation {\
                  createProfile (name:"' + name + '"){\
                    id\
                    name\
                  }\
                }';
            const req   = store.dispatch('getEntity', query);
            req.then((resp) => {
                arguments[0].state.profile = resp.data.data.createProfile;
            });
            return arguments[0].state.profile;
        },

        async generateDPS() {
            const [, data] = arguments;
            const query    = 'mutation {\
                generateDressPersonalStyle(id: ' + data.id + ') {\
                    clothes{\
                        name\
                    }\
                    hairstyle{\
                        name\
                    }\
                    affectations{\
                        name\
                    }\
                    ethnicOrigins{\
                        name\
                    }\
                }\
            }';
            await store.dispatch('getEntity', query);
        },

        async generateMotivations() {
            const [, data] = arguments;
            const query    = 'mutation {\
                  generateMotivations(id: ' + data.id + ') {\
                    motivationsLog {\
                      id\
                      name\
                      motivation{\
                        name\
                      }\
                    }\
                  }\
                }';

            await store.dispatch('getEntity', query);
        },
        async generateFb() {
            const [, data] = arguments;
            const query    = 'mutation{\
                  generateFb(id: ' + data.id + '){\
                    id\
                    name    \
                  }\
                }';
            await store.dispatch('getEntity', query);
        },
        async generateLifeEvents() {
            const [, data] = arguments;
            const query    = 'mutation{\
                  generateLifeEvents(id: ' + data.id + '){\
                    id\
                    name    \
                  }\
                }';
            await store.dispatch('getEntity', query);
        },
        async generateStats() {
            const [, data] = arguments;
            const query    = 'mutation{\
                  generateStats(id: ' + data.id + '){\
                    id\
                    name\
                  }\
                }';
            await store.dispatch('getEntity', query);
        },
    },
    getters   : {
        getProfile(state: any): IProfile {
            return state.profile;
        },
    },
};
