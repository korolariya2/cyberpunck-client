import store from '@/store';

export const FamilyStore = {
    namespaced: true,
    state     : {
        childhoods: [],
    },
    mutations : {
        inc(state: { count: number }) {
            // `state` указывает на локальное состояние модуля
            state.count++;
        },
    },
    actions   : {
        getChildhoods(): Promise<{}> {
            const req = store.dispatch('getEntity', '{getChildhoods{id name}}');

            req.then((resp) => {
                arguments[0].state.childhoods = resp.data.data;
            });

            return req;
        },

        getFamilyRank(): Promise<{}> {
            const req = store.dispatch('getEntity', '{getFamilyRank{id name}}');

            req.then((resp) => {
                arguments[0].state.childhoods = resp.data.data;
            });

            return req;
        },
    },
    getters   : {
        getChildhoods(state: any) {
            return state.childhoods;
        },
    },
};
