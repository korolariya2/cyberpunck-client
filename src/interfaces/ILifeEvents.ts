import {IBase} from '@/interfaces/IBase';
import {IBpBwLog} from '@/interfaces/IBpBwLog';
import {IFaeLog} from '@/interfaces/IFaeLog';
import {IRomanticLifeLog} from '@/interfaces/IRomanticLifeLog';

export interface ILifeEvents extends IBase {
    bpBwLog: IBpBwLog[];
    faeLog: IFaeLog[];
    romanticLifeLog: IRomanticLifeLog[];
}
