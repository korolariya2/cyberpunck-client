import {IBase} from '@/interfaces/IBase';
import {ILifeEvents} from '@/interfaces/ILifeEvents';

export interface IProfile extends IBase {
    age: string;
    sex: string;
    relationship: string;
    clothes: IBase;
    hairstyle: IBase;
    affectations: IBase;
    ethnicOrigins: IBase;
    motivationsLog: any;
    lifeEvents: ILifeEvents;
    siblings: IProfile[];
    stats: any;
}
