import {IBase} from '@/interfaces/IBase';

export interface IFaeLog extends IBase {
    fae: IBase;
}
