import {IBase} from '@/interfaces/IBase';

export interface IRomanticLifeLog extends IBase {
    romanticLife: IBase;
}
