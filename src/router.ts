import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Entities from './views/Entities.vue';
import Childhood from '@/components/childhood/Childhood';
import Generators from '@/components/generators/Generators';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path     : '/',
            name     : 'home',
            component: Home,
        },
        {
            path     : '/generators',
            name     : 'generators',
            component: Generators,
        },
        {
            path     : '/entities',
            name     : 'entities',
            redirect : {name: 'childhood'},
            component: Entities,
            children : [
                {
                    name     : 'childhood',
                    path     : 'childhood',
                    component: Childhood,
                },
                {
                    name     : 'familyRank',
                    path     : 'family-rank',
                    component: Childhood,
                },
            ],
        },
    ],
});
