import {Component, Vue} from 'vue-property-decorator';
import {mapGetters} from 'vuex';
import store from '@/store';

@Component
export default class Childhood extends Vue {
    public products      = [1, 2, 3];
    public computed: any = mapGetters({
        products: 'allProducts',
    });

    public childhoods: any  = [];

    constructor() {
        super();

        // this.ch = store.state.familyStore.childhoods;
        setTimeout(() => {
            // console.log(this.ch);
            // this.products.push(store.getters.doneTodosCount);
            // store.commit('increment',10);
            // store.commit('familyStore/inc');
            // console.log(store.getters['familyStore/doubleCount']);
            // console.log(store.state.familyStore.count);
            // axios.post('http://localhost:3000/graphql', {
            //     query: '{getChildhoods{id name}}',
            // }).then((r) => {
            //     console.log(r);
            // });
        }, 5000);
    }

    private async getCh() {
        await store.dispatch('familyStore/getChildhoods');
        this.childhoods = store.getters['familyStore/getChildhoods'];
    }

    private created() {
        this.getCh();
    }
}
