import {Component, Vue} from 'vue-property-decorator';
import store from '@/store';
import {IProfile} from '@/interfaces/IProfile';

@Component({
    computed: {
        profile() {
            return store.getters['profileStore/getProfile'];
        },
    },
})
export default class Generators extends Vue {

    public name: string = '';
    public id: string   = '';

    public profile: IProfile | undefined;

    constructor() {
        super();
    }

    public getStats() {
        if (this.profile != undefined && this.profile.stats && this.profile.stats.length) {
            return this.profile.stats[0];
        }
        return {};
    }

    public createProfile() {
        store.dispatch('profileStore/postProfile', {name: this.name});
    }

    public async loadProfile() {
        if (!this.id) {
            return;
        }
        await store.dispatch('profileStore/generateStats', {id: this.id});
        await store.dispatch('profileStore/generateDPS', {id: this.id});
        await store.dispatch('profileStore/generateFb', {id: this.id});
        await store.dispatch('profileStore/generateMotivations', {id: this.id});
        await store.dispatch('profileStore/generateLifeEvents', {id: this.id});
        await store.dispatch('profileStore/loadProfile', {id: this.id});
    }

    public generateDPS() {
        store.dispatch('profileStore/generateDPS');
    }

    public generateFb() {
        if (!this.id) {
            return;
        }
        store.dispatch('profileStore/generateFb', {id: this.id});
    }

    public generateMotivations() {
        store.dispatch('profileStore/generateMotivations', {id: this.id});
    }

    public generateLifeEvents() {
        if (!this.id) {
            return;
        }
        store.dispatch('profileStore/generateLifeEvents', {id: this.id});
    }
}
