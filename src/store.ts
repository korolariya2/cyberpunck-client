import Vue from 'vue';
import Vuex from 'vuex';
import {FamilyStore} from '@/modules/FamilyStore';
import axios from 'axios';
import {ProfileStore} from '@/modules/ProfileStore';

Vue.use(Vuex);

export const StoreConfig = {
    apiLink: 'http://localhost:3000/graphql',
};

export default new Vuex.Store({
    modules  : {
        familyStore: FamilyStore,
        profileStore: ProfileStore,
    },
    state    : {
        count: 1,
    },
    mutations: {
        increment(state, n) {
            // изменяем состояние
            state.count = n;
        },
    },
    getters  : {
    },
    actions  : {
        getEntity({}, query: string) {
            return axios.post('http://localhost:3000/graphql', {
                query,
            });
        },
    },
});
